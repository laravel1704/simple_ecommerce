<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['item_id', 'customer_email', 'customer_name', 'customer_phone', 'quantity'];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
