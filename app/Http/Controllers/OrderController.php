<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Mail\OrderConfirmation;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'item_id' => 'required|exists:items,id',
            'customer_email' => 'required|email',
            'customer_name' => 'required|string',
            'customer_phone' => 'required|string',
            'quantity' => 'required|integer',
        ]);

        $order = Order::create($data);

        Mail::to($order->customer_email)->send(new OrderConfirmation($order));


        return response()->json(['status'=>'True','message' => 'Order created successfully'], 201);
    }

    public function show($id)
    {
        $order = Order::findOrFail($id);

        return response()->json(['status'=>'True','order' => $order],200);
    }

    public function index()
    {
        $orders = Order::all();

        return response()->json(['status'=>'True','orders' => $orders],200);
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return response()->json(['status'=>'True','message' => 'Order deleted successfully'],200);
    }
}
