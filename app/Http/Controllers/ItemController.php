<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;

class ItemController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->validate([
            'category' => 'required|string',
            'subcategory' => 'required|string',
            'item_name' => 'required|string',
        ]);

        $item = Item::create($data);

        return response()->json(['status'=>'True','message' => 'Item created successfully'], 201);
    }

    public function show($id)
    {
        $item = Item::findOrFail($id);

        return response()->json(['status' => 'True','item' => $item]);
    }

    public function index()
    {
        $items = Item::all();

        return response()->json(['status'=>'True','items' => $items]);
    }
}

