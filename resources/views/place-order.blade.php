@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Place Your Order</h2>

        <div id="order-form">
        </div>
    </div>
    <style>
        .order-form {
            max-width: 400px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .item-details {
            text-align: center;
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 8px;
            color: #333;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 16px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }

        button {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            font-weight: bold;
            cursor: pointer;
        }
    </style>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const itemId = window.location.pathname.split('/').pop();
            fetchItemDetails(itemId);
        });

        function fetchItemDetails(itemId) {
            fetch(`/api/items/${itemId}`)
                .then(response => response.json())
                .then(data => displayOrderForm(data.item))
                .catch(error => console.error('Error fetching item details:', error));
        }

        function displayOrderForm(item) {
            const orderForm = document.getElementById('order-form');
            orderForm.innerHTML = `
                <div class="item-details">
                    <h3>${item.category} - ${item.subcategory}</h3>
                    <p>${item.item_name}</p>
                </div>
                <form id="place-order-form" onsubmit="submitOrder(event)">
                    <label for="customer_name">Name</label>
                    <input type="text" id="customer_name" name="customer_name" placeholder="Please enter your name" required>

                    <label for="customer_email">Email</label>
                    <input type="email" id="customer_email" name="customer_email" placeholder="Please enter your email" required>

                    <label for="customer_phone">Mobile Number</label>
                    <input type="tel" id="customer_phone" name="customer_phone" placeholder="Please enter your mobile number" required>

                    <label for="quantity">Quantity</label>
                    <input type="number" id="quantity" name="quantity" placeholder="Enter the quantity" required>

                    <button type="submit">Place Order</button>
                </form>
            `;
        }

        function submitOrder(event) {
            event.preventDefault();

            const itemId = window.location.pathname.split('/').pop();
            const customerName = document.getElementById('customer_name').value;
            const customerEmail = document.getElementById('customer_email').value;
            const customerPhone = document.getElementById('customer_phone').value;
            const quantity = document.getElementById('quantity').value;

            fetch('/api/create_order', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    item_id: itemId,
                    customer_name: customerName,
                    customer_email: customerEmail,
                    customer_phone: customerPhone,
                    quantity: quantity,
                }),
            })
                .then(response => response.json())
                .then(data => {
                    alert('Order placed successfully!');
                    window.location.href = '/items';
                })
                .catch(error => console.error('Error placing order:', error));
        }
    </script>
@endsection
