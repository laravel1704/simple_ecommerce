
@extends('layouts.app')

@section('content')
    <div class="container">
    <h2 style="text-align: center;">Shop Our Items</h2>

        <div id="items-list">
        </div>
    </div>

    <style>
        .item-card {
            border: 1px solid #ddd;
            border-radius: 8px;
            padding: 20px;
            margin: 20px 0;
            background-color: #fff;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }

        .place-order-btn {
            background-color: #4CAF50;
            color: #fff;
            padding: 10px;
            border: none;
            border-radius: 5px;
            font-weight: bold;
            cursor: pointer;
        }
    </style>


    <script>
        document.addEventListener('DOMContentLoaded', function () {
            fetchItems();
        });

        function fetchItems() {
            fetch('/api/items')
                .then(response => response.json())
                .then(data => displayItems(data.items))
                .catch(error => console.error('Error fetching items:', error));
        }

        function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function displayItems(items) {
            const itemsList = document.getElementById('items-list');
            itemsList.innerHTML = '';

            items.forEach(item => {
                const itemDiv = document.createElement('div');
                itemDiv.className = 'item-card';
                itemDiv.style.backgroundColor = getRandomColor();
                itemDiv.innerHTML = `
                    <h3>${item.category} - ${item.subcategory}</h3>
                    <p>${item.item_name}</p>
                    <button class="place-order-btn" onclick="placeOrder(${item.id})">Place Order</button>
                    <hr>
                `;
                itemsList.appendChild(itemDiv);
            });
        }

        function placeOrder(itemId) {
            window.location.href = `/place-order/${itemId}`;
        }
    </script>
@endsection
