<p>Dear {{ $order->customer_name }},</p>

<p>Your order has been confirmed with the following details:</p>

<p>Item: {{ $order->item->item_name }}</p>
<p>Quantity: {{ $order->quantity }}</p>

<p>Thank you for your order!</p>
